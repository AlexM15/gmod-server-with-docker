#!/bin/sh

if [ $# -ne 2 ]; then
    echo "gmodServer.sh [start|stop|switch] [gamemodeFolder]"
    exit 1
fi

if [ "$1" = "start" ]; then
    cd "$2"

    echo "Serveur en cours de lancement..."

    # Start the Docker Compose stack in detached mode and save the logs to a file
    docker-compose up -d 
    docker-compose logs -f -t --tail 5 garrysmod > gmodServerLogs.log 2>&1 &

    # Wait for the stack to start up
    sleep 5

    # Define the pattern to search for in the logs
    PATTERN="Server will auto-restart if there is a crash."

    # Continuously monitor the logs for the pattern
    while true
    do
    if grep -q "$PATTERN" gmodServerLogs.log; then
        echo "Serveur lancé !"
        break
    fi
    sleep 1
    done

elif [ "$1" = "stop" ]; then
    cd "$2"
    echo "Arrêt du serveur en cours..."

    docker-compose stop

    echo "Serveur arrêté !"
    exit 1

elif [ "$1" = "switch" ]; then
    cd "$2"
    echo "Arrêt du serveur en cours..."

    docker rm -f gmod_server
    echo "Ancien serveur arrêté !"
    
    echo "Nouveau serveur en cours de lancement..."

    # Start the Docker Compose stack in detached mode and save the logs to a file
    docker-compose up -d 
    docker-compose logs -f -t --tail 5 garrysmod > gmodServerLogs.log 2>&1 &

    # Wait for the stack to start up
    sleep 5

    # Define the pattern to search for in the logs
    PATTERN="Server will auto-restart if there is a crash."

    # Continuously monitor the logs for the pattern
    while true
    do
    if grep -q "$PATTERN" gmodServerLogs.log; then
        echo "Serveur lancé !"
        break
    fi
    sleep 1
    done
    exit 1
else
    echo "Le paramètre d'action '$1' est invalide"
    echo "Les paramètres d'action sont: start, stop ou switch"
    exit 1
fi