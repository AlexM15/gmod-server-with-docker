# Gmod Server Avec Docker

Voici un petit projet rapide pour lancer un serveur Garry's mod via docker.

Pour pouvoir utiliser ce projet, vous allez devoir récupérer plusieurs éléments au préalable:  

**1- Installer Docker et Docker compose**

**2- Récupérez la [clé d'authentification](http://steamcommunity.com/dev/apikey) de votre compte steam**

**3- Notez votre clé d'authentification dans le fichier authkey.env**

**4- Créez-vous une collection de map pour le mode de jeu que vous souhaitez via le workshop**

**5- Récupérez l'Id la collection que vos souhaitez relier au mode de jeu et notez le dans le fichier environment.env sur "WORKSHOP" et "WORKSHOPDL" (ainsi que le mode de jeu)**

Si vous ne savez pas le nom du mode de jeu **dans le jeu**, lancez garry's mod et mettez vous en sandbox, puis ouvrez la console et changez de mode de jeu en bas à droite de votre écran, vous devriez voir le nom du mode de jeu apparaitre en bas de la console

**6- Renommez le dossier "gamemode_examples" avec le nom du gamemode configuré** 

**7- Enfin, modifiez le fichier server.cfg sur "hostname", "rcon_password" et "sv_password" pour modifier respectivement le nom du serveur, le mot de passe administrateur (pour l'accès au changement de map et autres commandes in-game) et le mot de passe de connexion au serveur**

## Il ne vous reste plus qu'à lancer le server en éxécutant le script gmodServer.sh en lui donnant en paramètre l'action et le mode de jeu :

    sudo bash ./gmodServer.sh start|stop|switch gamemode_name

## Et à vous connectez au serveur via la commande suivante :

    connect [insérez l'ip du serveur]; password [insérez le mot de passe du connexion au serveur]


**Quelques commandes in-game utiles**

Pour avoir accès au commande admin depuis la console sur le serveur

    rcon_password_ [PASSWORD] 

Pour changer de map (nécessite l'accès au commande admin)

    rcon changelevel [insérez le nom de la map]


Ce projet a été créé en se basant sur le projet docker de [hackebein/garrysmod](https://hub.docker.com/r/hackebein/garrysmod/)

Pour plus d'informations, n'hésitez pas à regarder sa documentation ou à me contacter.
